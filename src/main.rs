// https://docs.rs/fltk/0.9.2/fltk/
// https://crates.io/crates/fltk
use fltk::{app::*, window::*, frame::*, button::*};

static INITIAL_APP_WIDTH: i32 = 1000;
static INITIAL_APP_HEIGHT: i32 = 1000;

enum Cell {
    Mine,
    Clear(u32),
}

struct Square {
    cell_type: Cell,
    button: Button,
    visible: bool,
}

impl Square {
    pub fn new(cell_type: Cell, mut button: Button) -> Square {
        Square {
            cell_type,
            button,
            visible: false,
        }
    }

    pub fn button(&mut self) -> &mut Button {
        &mut self.button
    }
}

fn main() {
    let mine_count = 40;
    let field_width = 100;
    let field_height = 16;

    let app = App::default().with_scheme(AppScheme::Base);
    let mut window = Window::new(
        100, 100, 
        INITIAL_APP_WIDTH, INITIAL_APP_HEIGHT, 
        "Hello, world!");

    let button_width = window.width() / field_width;
    let button_height = window.height() / field_height;
    let mut squares: Vec<Vec<Square>> = Vec::with_capacity(field_height as usize);
    let mut button_count = 0;
    for i in 0..field_height {
        let mut vec = Vec::with_capacity(field_width as usize);

        for j in 0..field_width {
            button_count += 1;
            let mut button = Button::new(
                j * button_width, i * button_height,
                button_width, button_height,
                format!("{},{}", i, j).as_str());
 
            vec.push(Square::new(Cell::Clear(0), button));
        }

        squares.push(vec);

    }

    println!("{} children.", window.children());
    println!("{} buttons.", button_count);

    window.end();
    window.show();
    window.make_resizable(true);

    let (s, r) = fltk::app::channel();
    //button.emit(s, 1);
    //let mut counter = 0;
    
    for vec in squares {
        for mut square in vec {
            let mut button = square.button;
            let xpos = button.x();
            let ypos = button.y();
            button.emit(s, (xpos, ypos));
        }
    }

    while app.wait().unwrap() {
        match r.recv() {
            Some((x, y)) => {
                println!("Pressed button {},{}.", x, y);
                window.resize(x, y, 720, 720);
            },
            None => (),
        }
    }

    //button.set_callback(Box::new(move || {
    //    counter += 1;
    //    frame.set_label(format!("{}", counter).as_str())
    //}));

    //app.run().unwrap();
}

//fn winit() {
//    // https://docs.rs/winit/0.22.2/winit/
//    use winit::event_loop::{ControlFlow, EventLoop};
//    use winit::window::WindowBuilder;
//    use winit::event::{Event, WindowEvent};
//
//    use std::thread;
//    use std::time::Duration;
//    let event_loop = EventLoop::new();
//    let window = WindowBuilder::new()
//        .with_visible(false)
//        .build(&event_loop)
//        .expect("Failed to open window.");
//
//    event_loop.run(move |event, _, control_flow| {
//        //*control_flow = ControlFlow::Poll; // always run
//        *control_flow = ControlFlow::Wait; // pause until input
//
//        match event {
//            Event::WindowEvent {
//                event: WindowEvent::CloseRequested,
//                ..
//            } => {
//                println!("The close button was pressed; stopping");
//                *control_flow = ControlFlow::Exit
//            },
//            //Event::MainEventsCleared => {
//            //    window.request_redraw();
//            //},
//            Event::RedrawRequested(_) => {
//            },
//            _ => ()
//        }
//    });
//}
